class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :name, index: true
      t.string :specialization, index: true
      t.string :city, index: true
      t.text :about_me

      t.timestamps null: false
    end
  end
end
