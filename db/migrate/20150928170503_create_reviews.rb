class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.references :doctor, index: true, foreign_key: true, null: false
      t.string :author
      t.text :body

      t.timestamps null: false
    end
  end
end
