class CreateSlots < ActiveRecord::Migration
  def change
    create_table :slots do |t|
      t.references :doctor, index: true, foreign_key: true, null: false
      t.references :booking, index: true, foreign_key: true
      t.datetime :from, null: false
      t.datetime :to, null: false

      t.timestamps null: false
    end
  end
end
