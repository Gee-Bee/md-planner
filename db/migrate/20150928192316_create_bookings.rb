class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.string :patient_name, null: false

      t.timestamps null: false
    end
  end
end
