Swaggard.configure do |config|
   config.api_formats = [:json]
   config.title = 'Doctor appointment service'
   config.description = 'This service will allow you to browse a doctors catalogue, schedule an appointment and make a review of a doctor.'
 end
