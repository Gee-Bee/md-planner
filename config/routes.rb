Rails.application.routes.draw do
  mount Swaggard::Engine, at: '/swagger'
  root to: redirect('/swagger')
  resources :doctors, except: [:new, :edit] do
    resources :reviews, except: [:new, :edit]
    resources :slots, except: [:new, :edit] do
      post :book, on: :member
    end
    resources :bookings, except: [:new, :create, :edit, :update]
  end
end
