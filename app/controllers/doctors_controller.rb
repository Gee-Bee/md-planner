class DoctorsController < ApplicationController
  before_action :set_doctor, only: [:show, :update, :destroy]

  # Returns the list of doctors
  #
  # @query_parameter [string] specialization specialization of a doctor
  # @query_parameter [string] city
  #
  # @reponse_class Array<DoctorSerializer>
  def index
    @doctors = Doctor.all
    @doctors = @doctors.where(specialization: doctor_params[:specialization]) if doctor_params[:specialization].present?
    @doctors = @doctors.where(city: doctor_params[:city]) if doctor_params[:city].present?
    render json: @doctors
  end

  # Show doctor
  #
  # @response_class DoctorSerializer
  def show
    render json: @doctor
  end

  # Create doctor
  #
  # @body_parameter [string] name
  # @body_parameter [string] specialization
  # @body_parameter [string] city
  # @body_parameter [string] about_me
  #
  # @response_class DoctorSerializer
  def create
    @doctor = Doctor.new(doctor_params)
    if @doctor.save
      render json: @doctor, status: :created, location: @doctor
    else
      render json: @doctor.errors, status: :unprocessable_entity
    end
  end

  # Update doctor
  #
  # @body_parameter [string] name
  # @body_parameter [string] specialization
  # @body_parameter [string] city
  # @body_parameter [string] about_me
  #
  # @response_class DoctorSerializer
  def update
    @doctor = Doctor.find(params[:id])
    if @doctor.update(doctor_params)
      head :no_content
    else
      render json: @doctor.errors, status: :unprocessable_entity
    end
  end

  # Destroy doctor
  def destroy
    @doctor.destroy

    head :no_content
  end

  private

    def set_doctor
      @doctor = Doctor.find(params[:id])
    end

    def doctor_params
      params.permit(:name, :specialization, :city, :about_me)
    end
end
