class BookingsController < ApplicationController
  before_action :set_doctor
  before_action :set_booking, only: [:show, :destroy]

  # Returns the list of bookings for given doctor
  #
  # @query_parameter [datetime] from
  # @query_parameter [datetime] to
  #
  # response_class Array<BookingSerializer>
  def index
    from = search_params[:from] && Time.zone.parse(search_params[:from]) || Time.zone.now
    to = search_params[:to] && Time.zone.parse(search_params[:to]) || from + 1.month
    @bookings = Booking.between(from, to)

    render json: @bookings.order('"slots.from"')
  end

  # Show booking
  #
  # @response_class SlotSerializer
  def show
    render json: @booking
  end

  # Cancel booking
  def destroy
    @booking.destroy

    head :no_content
  end

  private

    def set_doctor
      @doctor = Doctor.find(params[:doctor_id])
    end

    def set_booking
      @booking = Booking.find(params[:id])
    end

    def search_params
      params.permit(:from, :end)
    end
end
