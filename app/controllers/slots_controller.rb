class SlotsController < ApplicationController
  before_action :set_doctor
  before_action :set_slot, only: [:show, :update, :destroy, :book]

  # Returns the list of slots
  #
  # @query_parameter [datetime] from
  # @query_parameter [datetime] to
  #
  # @reponse_class Array<SlotSerializer>
  def index
    from = slot_params[:from] && Time.zone.parse(slot_params[:from]) || Time.zone.now
    to = slot_params[:to] && Time.zone.parse(slot_params[:to]) || from + 1.month
    @slots = Slot.between(from, to)
    
    render json: @slots.order(:from)
  end

  # Show slot
  #
  # @response_class SlotSerializer
  def show
    render json: @slot
  end

  # Create slot
  #
  # @body_parameter [datetime] from
  # @body_parameter [datetime] to
  #
  # @response_class SlotSerializer
  def create
    @slot = Slot.new(slot_params)
    @slot.to = @slot.from + 1.hour if @slot.from && slot_params[:to].blank?

    if @slot.save
      render json: @slot, status: :created, location: [@doctor, @slot]
    else
      render json: @slot.errors, status: :unprocessable_entity
    end
  end

  # Update slot
  #
  # @body_parameter [datetime] from
  # @body_parameter [datetime] to
  #
  # @response_class SlotSerializer
  def update
    @slot = Slot.find(params[:id])

    if @slot.update(slot_params)
      head :no_content
    else
      render json: @slot.errors, status: :unprocessable_entity
    end
  end

  # Destroy slot
  def destroy
    @slot.destroy

    head :no_content
  end

  # Book slot
  #
  # @body_parameter [string] patient_name
  #
  # @response_class BookingSerializer
  def book
    @booking = @slot.book(booking_params)
    if @slot.errors.empty? && @slot.save
      render json: @booking, status: :created, location: [@doctor, @booking]
    else
      render json: @slot.errors, status: :unprocessable_entity
    end
  end

  private

    def set_doctor
      @doctor = Doctor.find(params[:doctor_id])
    end

    def set_slot
      @slot = @doctor.slots.find(params[:id])
    end

    def slot_params
      params.permit(:from, :to)
    end

    def booking_params
      params.permit(:patient_name)
    end
end
