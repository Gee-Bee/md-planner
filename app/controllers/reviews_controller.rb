class ReviewsController < ApplicationController
  before_action :set_doctor
  before_action :set_review, only: [:show, :update, :destroy]

  # Return reviews for given doctor
  #
  # @response_class Array<ReviewSerializer>
  def index
    @reviews = Review.all

    render json: @reviews
  end

  # Show review
  #
  # @response_class ReviewSerializer
  def show
    render json: @review
  end

  # Create review
  #
  # @body_parameter [string] author
  # @body_parameter [text] body
  #
  # @response_class ReviewSerializer
  def create
    @review = Review.new(review_params)

    if @review.save
      render json: @review, status: :created, location: [@doctor, @review]
    else
      render json: @review.errors, status: :unprocessable_entity
    end
  end

  # Update review
  #
  # @body_parameter [string] author
  # @body_parameter [text] body
  #
  # @response_class ReviewSerializer
  def update
    @review = Review.find(params[:id])

    if @review.update(review_params)
      head :no_content
    else
      render json: @review.errors, status: :unprocessable_entity
    end
  end

  # Destroy review
  def destroy
    @review.destroy

    head :no_content
  end

  private

    def set_doctor
      @doctor = Doctor.find(params[:doctor_id])
    end

    def set_review
      @review = @doctor.reviews.find(params[:id])
    end

    def review_params
      params.permit(:doctor_id, :author, :body)
    end
end
