class Booking < ActiveRecord::Base
  has_one :slot, dependent: :nullify

  scope :between, -> (from, to) { joins(:slot).merge(Slot.between(from, to)) }

  validates :slot, presence: true
  validates :patient_name, presence: true
end
