class Slot < ActiveRecord::Base
  belongs_to :doctor
  belongs_to :booking, dependent: :destroy, autosave: true

  scope :between, -> (from, to) { where('"slots"."from" >= ? and "slots"."to" <= ?', from, to) }

  validates :doctor, presence: true
  validates :from, presence: true, date: {after: Proc.new {Time.zone.now}}
  validates :to, presence: true, date: {after: :from}
  validate :no_overlapping

  def book booking_params
    if booking
      errors.add(:base, 'already_booked')
      nil
    else
      build_booking booking_params
    end
  end

  private

    def overlapping?
      scope = self.class.where(doctor_id: doctor.id)
      scope = scope.where('"id" != ?', id) if persisted?
      scope.exists?(['"from" < ? and "to" > ?', to, from])
    end

    def no_overlapping
      errors.add(:base, 'slot is overlapping') if overlapping?
    end
end
