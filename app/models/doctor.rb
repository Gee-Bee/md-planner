class Doctor < ActiveRecord::Base
  has_many :reviews, dependent: :destroy
  has_many :slots, dependent: :destroy

  validates :name, presence: true
  validates :specialization, presence: true, inclusion: {in: %w(Ortopeda Stomatolog Chirurg)}
  validates :city, presence: true, inclusion: {in: %w(Warszawa Poznań)}
end
