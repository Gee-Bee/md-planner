class Review < ActiveRecord::Base
  belongs_to :doctor

  validates :doctor, presence: true
  validates :author, presence: true
  validates :body, presence: true
end
