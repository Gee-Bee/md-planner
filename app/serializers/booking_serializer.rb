# @attr [datetime] patient_name
# @attr [SlotSerializer] slot
class BookingSerializer < ActiveModel::Serializer
  attributes :id, :patient_name
  has_one :slot
end
