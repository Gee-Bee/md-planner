# @attr [datetime] from
# @attr [datetime] to
# @attr [DoctorSerializer] doctor
# @attr [BookingSerializer] booking
class SlotSerializer < ActiveModel::Serializer
  attributes :id, :from, :to
  has_one :doctor
  has_one :booking
end
