# @attr [string] author
# @attr [string] body
class ReviewSerializer < ActiveModel::Serializer
  attributes :id, :author, :body
  has_one :doctor
end
