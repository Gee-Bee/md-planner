# @attr [string] name
# @attr [string] specialization
# @attr [string] city
# @attr [text] about_me
class DoctorSerializer < ActiveModel::Serializer
  attributes :id, :name, :specialization, :city, :about_me
end
